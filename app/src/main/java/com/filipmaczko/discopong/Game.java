package com.filipmaczko.discopong;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class Game extends Activity {

    private GameView gv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(gv==null){
            gv = new GameView(this);
            setContentView(gv);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        gv.unpause();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gv.pause();
    }
}
