package com.filipmaczko.discopong;

import android.app.Activity;
import android.graphics.Typeface;
import android.widget.TextView;

public class ColoredText implements ChangeableColor {

    private Activity context;
    private TextView myText;

    public ColoredText(Activity context, TextView myText){
        this.myText = myText;
        this.context = context;
    }

    public void setTypeFace(Typeface t){
        myText.setTypeface(t);
    }

    @Override
    public void changeColor(final int colorCode) {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                    myText.setTextColor(colorCode);
            }
        });
    }
}
