package com.filipmaczko.discopong;

public enum GameState {
    ENDED, PAUSED, RUNNING, POINT_SCORED,CALIBRATION
}
