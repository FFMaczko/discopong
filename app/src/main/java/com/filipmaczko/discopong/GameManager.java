package com.filipmaczko.discopong;

import android.app.Activity;
import android.graphics.Canvas;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.Random;

import static android.content.Context.SENSOR_SERVICE;

public class GameManager {

    //ui
    private UIMenager uiMenager;

    //sounds and vibrations
    private SVMenager svMenager;

    //game informations
    private final int paddleSpeed;
    private final int maxBallSpeed;
    private final int numberOfBounces=2;
    private int move;
    private int ballSpeedX;
    private int ballSpeedY;
    private int playerDirection=1;
    private int randomNumber;
    private Random randomGenerator = new Random();

    //game objects
    private DrawableRect ball;
    private DrawableRect player;
    private DrawableRect enemy;

    //colisions
    private static int COLLISION_DELAY =30;
    private int enemyCollide = 0, paddleCollide= 30;

    //
    private int enemySpeed;

    //score
    private int playerScore=0;
    private int computerScore =0;
    private GameState gameState = GameState.CALIBRATION;
    private final int RESUME_TIME = 60;
    private int resumeCounter = RESUME_TIME;

    //sensors
    private SensorManager sensorManager;
    private  float MINIMAL_LIGHT = 0;
    private float lightValue=0;
    private final int CALIBRATION = 60;
    private int calibrationDuration = CALIBRATION;
    private Sensor lightSensor;
    private SensorEventListener lightSensorListener;

    public GameManager(Activity context){
        sensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);;
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        lightSensorListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                lightValue = sensorEvent.values[0];
            }
            @Override
            public void onAccuracyChanged(Sensor sensor, int i) { } //deliberatly empty method
        };

        svMenager = new SVMenager(context);
        uiMenager = new UIMenager(context);
        enemy = new DrawableRect(uiMenager.screenWidth()/2 - uiMenager.getPaddleX()/2, 2* uiMenager.getPaddleY(), uiMenager.getPaddleX(), uiMenager.getPaddleY());
        player = new DrawableRect(uiMenager.screenWidth()/2 - uiMenager.getPaddleX()/2, uiMenager.screenHeight() - uiMenager.getPaddleY() * 3 ,uiMenager.getPaddleX(), uiMenager.getPaddleY());
        ball = new DrawableRect(uiMenager.screenWidth()/2 - uiMenager.getBallX(), uiMenager.screenHeight()/2 - uiMenager.getBallY()/2, uiMenager.getBallX(), uiMenager.getBallY());
        // setting up ui
        uiMenager.registerObjectToDraw(enemy);
        uiMenager.registerObjectToDraw(player);
        uiMenager.registerObjectToDraw(ball);
        uiMenager.registerColoredObject(enemy);
        uiMenager.registerColoredObject(player);
        uiMenager.registerColoredObject(ball);

        paddleSpeed = uiMenager.getPaddleX()/40;
        ballSpeedX = paddleSpeed;
        maxBallSpeed = (uiMenager.getBallY()/5 + 1) * 4;
        ballSpeedY = -uiMenager.getBallY()/5 + 1;
    }


    // MOVEMENT
    private void moveBall(){
        ball.setX(ball.getX()+ballSpeedX);
        ball.setY(ball.getY()+ballSpeedY);
    }

    private void moveEnemy(){
        enemySpeed = (paddleSpeed * 2) /3 +(int) ((float)(playerScore-computerScore)/10.0f * (float) paddleSpeed);
        if(enemySpeed<=enemySpeed/2 && enemySpeed/2 > 0){ enemySpeed = enemySpeed/2; }
        else if (enemySpeed == 0 ){ enemySpeed = 1; }

        if(ball.getY()<uiMenager.screenHeight()) {

            if (ball.getX() > enemy.getX()) {
                enemy.setX(enemy.getX() + enemySpeed);
            } else {
                enemy.setX(enemy.getX() - enemySpeed);
            }
        }
        else{
            if(enemy.getX()+uiMenager.getPaddleX()/2 < uiMenager.screenWidth()/2){
                enemy.setX(enemy.getX() + enemySpeed);
            }
            else{
                enemy.setX(enemy.getX() - enemySpeed);
            }
        }
    }

    private void movePlayer(){
        if(lightValue > MINIMAL_LIGHT/2){ playerDirection = -1; }
        else{ playerDirection = 1; }
        player.setX(player.getX() + paddleSpeed * playerDirection);
    }
    //========================================================================


    //COLLISION DETECTION
    private void detectEnemyColision(){
        if(enemy.getX()> uiMenager.screenWidth() - uiMenager.getPaddleX()){
            enemy.setX(uiMenager.screenWidth() - uiMenager.getPaddleX());
        }
        if(enemy.getX() < 0){
            enemy.setX(0);
        }
    }

    private void detectPlayerColision(){
        if(player.getX() > uiMenager.screenWidth() - uiMenager.getPaddleX()){
            player.setX(uiMenager.screenWidth() - uiMenager.getPaddleX());
        }
        if(player.getX() < 0){
            player.setX(0);
        }
    }

    private void detectBallCollision(){

        //WALLS
        if(ball.getX() > uiMenager.screenWidth() - uiMenager.getBallX()){
             ball.setX(uiMenager.screenWidth()- uiMenager.getBallX());
             ballSpeedX*=-1;
        }
        if(ball.getX() < 0){
            ball.setX(0);
            ballSpeedX*=-1;
        }
        if(ball.getY() <= 0){
            score();
            playerScore+=1;
        }
        if(ball.getY() >= uiMenager.screenHeight() - uiMenager.getBallY()){
            score();
            computerScore +=1;
        }

        //PLAYER
        if(ball.getY() + uiMenager.getBallY() >= player.getY() && ball.getY() <= player.getY()+ uiMenager.getPaddleY() && paddleCollide<=0){
            if(ball.getX()<=player.getX()+ uiMenager.getPaddleX() && ball.getX()+ uiMenager.getBallX() >= player.getX()){
                ballSpeedY*=-1;
                paddleCollide = COLLISION_DELAY;
                collideWithPaddle();
            }
            if(ball.getX() + uiMenager.getBallX() >= player.getX() && ball.getX() <= player.getX()){
                ballSpeedX=-ballSpeedX;
                paddleCollide = COLLISION_DELAY;
                collideWithPaddle();
            }
            if(ball.getX() <= player.getX() + uiMenager.getPaddleX() && ball.getX() + uiMenager.getBallX() >= player.getX() + uiMenager.getPaddleX() ){
                ballSpeedX=-ballSpeedX;
                paddleCollide = COLLISION_DELAY;
                collideWithPaddle();
            }
        }


        //ENEMY
        if(ball.getY() + uiMenager.getBallY() >= enemy.getY() && ball.getY() <= enemy.getY()+ uiMenager.getPaddleY() && enemyCollide<=0){
            if(ball.getX()<=enemy.getX()+ uiMenager.getPaddleX() && ball.getX()+ uiMenager.getBallX() >= enemy.getX()){
                collideWithPaddle();
                ballSpeedY*=-1;
                enemyCollide = COLLISION_DELAY;
            }
            if(ball.getX() + uiMenager.getBallX() >= enemy.getX() && ball.getX() <= enemy.getX()){
                collideWithPaddle();
                ballSpeedX=-ballSpeedX;
                enemyCollide = COLLISION_DELAY;
            }
            if(ball.getX() <= enemy.getX() + uiMenager.getPaddleX() && ball.getX() + uiMenager.getBallX() >= enemy.getX() + uiMenager.getPaddleX() ){
                collideWithPaddle();
                ballSpeedX=-ballSpeedX;
                enemyCollide = COLLISION_DELAY;
            }
        }
        if(enemyCollide>0){ enemyCollide-=1; }
        if(paddleCollide>0) { paddleCollide-=1; }
    }

    private void collideWithPaddle(){
        randomNumber = randomGenerator.nextInt(Math.max(ballSpeedY/4, maxBallSpeed/16));
        if(Math.abs(ballSpeedX) > Math.abs(ballSpeedY)){
            ballSpeedY += randomNumber * (ballSpeedY/Math.abs(ballSpeedY));
            ballSpeedX -= randomNumber * (ballSpeedX/Math.abs(ballSpeedX));
        }
        else{
            ballSpeedY -= randomNumber * (ballSpeedY/Math.abs(ballSpeedY));
            ballSpeedX += randomNumber * (ballSpeedX/Math.abs(ballSpeedX));
        }
        move+=1;
        if(move==2*numberOfBounces){
            move=0;
            if(Math.abs(ballSpeedY)<maxBallSpeed)
            ballSpeedY+= Math.abs(ballSpeedY)/ballSpeedY;
        }
        svMenager.bounce();
    }
    private void score(){
        gameState =GameState.POINT_SCORED;
        svMenager.point();
    }

    private void resetGameState(){
        move=0;
        ballSpeedY=-maxBallSpeed/4;
        ballSpeedX=maxBallSpeed/4;
        ball.setX(uiMenager.screenWidth()/2 - uiMenager.getBallX() /2);
        ball.setY(uiMenager.screenHeight()/2 - uiMenager.getBallY() /2);
        enemy.setX(uiMenager.screenWidth()/2 - uiMenager.getPaddleX()/2);
        player.setX(uiMenager.screenWidth()/2 - uiMenager.getPaddleX()/2);
    }

    public void update(){
        if(gameState==GameState.RUNNING) {
            movePlayer();
            moveEnemy();
            moveBall();
            detectBallCollision();
            detectPlayerColision();
            detectEnemyColision();
        }
        else if(gameState==GameState.POINT_SCORED){
            resumeCounter -= 1;
            if(resumeCounter<=0){
                resetGameState();
                resumeCounter = RESUME_TIME;
                gameState = GameState.RUNNING;
            }
        }
        else if(gameState==GameState.CALIBRATION){
            calibrate();
        }
        uiMenager.changeColors();

    }

    private void calibrate(){
        calibrationDuration-=1;
        MINIMAL_LIGHT += lightValue;
        if(calibrationDuration==0){
            MINIMAL_LIGHT = MINIMAL_LIGHT/CALIBRATION;

            gameState=GameState.RUNNING;
        }
    }

    public void drawObjects(Canvas canvas) {
        uiMenager.drawObjects(canvas);
        if(gameState == GameState.POINT_SCORED) {
            uiMenager.showScore(computerScore,playerScore,canvas);
        }
        else if(gameState==GameState.CALIBRATION) {
            uiMenager.showCalibration(canvas);
        }
    }

    public void turnOffSensors(){
        sensorManager.unregisterListener(lightSensorListener);
    }
    public void turnOnSensoes() {sensorManager.registerListener(lightSensorListener,lightSensor,SensorManager.SENSOR_DELAY_GAME);}
    public void pauseGame(){ gameState = GameState.PAUSED; }
    public void unpauseGame(){ if(gameState == GameState.PAUSED) gameState = GameState.RUNNING; }

}