package com.filipmaczko.discopong;

import android.graphics.Canvas;

public class LayoutChanger extends  Thread{

    private Changeable layout;
    private boolean running;
    public static Canvas canvas;
    long targetFPS = 60;
    long startTime;
    long timeMillis;
    long waitTime;
    long totalTime = 0;
    int frameCount = 0;
    long targetTime = 1000 / targetFPS;


    public LayoutChanger(Changeable l) {

        super();
        this.layout = l;

    }

    @Override
    public void run() {
        long averageFPS;
        while (running) {
            startTime = System.nanoTime();
            canvas = null;

            try {

                    this.layout.run();

            } catch (Exception e) {       }
            finally {
                if (canvas != null)            {
                    try {

                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            timeMillis = (System.nanoTime() - startTime) / 1000000;
            waitTime = targetTime - timeMillis;

            try {
                this.sleep(waitTime);
            } catch (Exception e) {}

            totalTime += System.nanoTime() - startTime;
            frameCount++;
            if (frameCount == targetFPS)        {
                averageFPS = 1000 / ((totalTime / frameCount) / 1000000);
                frameCount = 0;
                totalTime = 0;
                System.out.println(averageFPS);
            }
        }

    }

    public void setRunning(boolean isRunning) {
        running = isRunning;
    }
}
