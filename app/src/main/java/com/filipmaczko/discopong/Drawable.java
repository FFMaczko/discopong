package com.filipmaczko.discopong;

import android.graphics.Canvas;

public interface Drawable {
    public void draw(Canvas canvas);
}
