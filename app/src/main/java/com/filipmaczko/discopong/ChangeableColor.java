package com.filipmaczko.discopong;

public interface ChangeableColor {
    public void changeColor(int colorCode);
}
