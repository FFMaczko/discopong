package com.filipmaczko.discopong;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.LinkedList;

public class MainMenu extends FragmentActivity implements Changeable {

    private static LayoutChanger lc = null;
    private static ColorChanger colorChanger = new ColorChanger(5);
    private static Typeface typeface;
    private LinkedList<Fragment> menuFragments;
    public static FragmentActivity activity;
    public int loadedFragment=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        activity = this;

        menuFragments = new LinkedList<Fragment>();
        AutorsFragment a = new AutorsFragment();
        a.setParent(this);
        menuFragments.addLast(a);

        MainMenuFragment m =new MainMenuFragment();
        menuFragments.addLast(m);
        m.setParent(this);

        HtpFragment h = new HtpFragment();
        h.setParent(this);
        menuFragments.addLast(h);

        typeface = Typeface.createFromAsset(this.getAssets(),"font/bb.ttf");
        if(lc==null) {
            lc = new LayoutChanger(this);
            lc.setRunning(true);
            lc.start();
        }

        selectFragment(1);

        FrameLayout fc = findViewById(R.id.frame);
        fc.setOnTouchListener(new OnSwipeTouchListener(MainMenu.this) {
            public void onSwipeTop() {

            }
            public void onSwipeRight() {
                if(loadedFragment>=1) {
                    loadedFragment -= 1;
                }
                selectFragment(loadedFragment);
            }
            public void onSwipeLeft() {
                if(loadedFragment<menuFragments.size()-1) {
                    loadedFragment += 1;
                }
                selectFragment(loadedFragment);
            }
            public void onSwipeBottom() {

            }

        });

    }


    public static void registerText(TextView t){
        ColoredText c =new ColoredText(activity, t);
        colorChanger.registerColoredObject(c);
        c.setTypeFace(typeface);
    }

    public void selectFragment(int i){

        this.getSupportFragmentManager().beginTransaction().replace(R.id.frame,menuFragments.get(i)).commit();
    }

    @Override
    public void run(){
        colorChanger.changeColors();
    }

    @Override
    public void finish() {
        if (loadedFragment == 1) {
            boolean retry = true;
            while (retry) {
                try {
                    lc.setRunning(false);
                    lc.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                retry = false;
            }
            lc = null;
            super.finish();
        }
        else{
            loadedFragment=1;
            selectFragment(1);
        }
    }
}
