package com.filipmaczko.discopong;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AutorsFragment extends Fragment {
    MainMenu parent;

    public void setParent(MainMenu parent)
    {
        this.parent=parent;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragemnt_autors, parent, false);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Display display = MainMenu.activity.getWindowManager().getDefaultDisplay();
        Point windowSize = new Point();
        display.getSize(windowSize);

        TextView a1 = getView().findViewById(R.id.a1);
        MainMenu.registerText(a1);
        if(windowSize.x<1080)   a1.setTextSize(65.0f * (float)(windowSize.x)/1080.0f);
        TextView a2 = getView().findViewById(R.id.a2);
        if(windowSize.x<1080)   a2.setTextSize(65.0f * (float)(windowSize.x)/1080.0f);
        MainMenu.registerText(a2);
        TextView menu = getView().findViewById(R.id.menuButton);
        MainMenu.registerText(menu);
        menu.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.loadedFragment=1;
                parent.selectFragment(1);
            }
        });

    }

}
