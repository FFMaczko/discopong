package com.filipmaczko.discopong;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.view.Display;

import java.util.LinkedList;

public class UIMenager {
    private Activity gameActivity;
    private Point windowSize;
    private final int ballX;
    private final int ballY;
    private final int paddleX;
    private final int paddleY;
    private final float textSize;
    private Paint scoreTextPain;
    private Paint calibrationTextPain;

    private ColorChanger colorChanger = new ColorChanger(5);
    private LinkedList<Drawable> objectsToDraw = new LinkedList<Drawable>();

    public UIMenager(Activity context){
        gameActivity = context;
        Display display = gameActivity.getWindowManager().getDefaultDisplay();
        windowSize = new Point();
        display.getSize(windowSize);
        ballX = screenWidth()/16;
        ballY = ballX;
        paddleX = (screenWidth()/8) * 3;
        paddleY = (screenWidth()/8);
        textSize = screenWidth()/5;


        scoreTextPain = new Paint();
        scoreTextPain.setColor(Color.WHITE);
        scoreTextPain.setTextSize(textSize);
        scoreTextPain.setTextAlign(Paint.Align.CENTER);
        scoreTextPain.setTypeface(Typeface.createFromAsset(context.getAssets(),"font/bb.ttf"));

        calibrationTextPain = new Paint();
        calibrationTextPain.setColor(Color.WHITE);
        calibrationTextPain.setTextSize(textSize/3);
        calibrationTextPain.setTextAlign(Paint.Align.CENTER);
        calibrationTextPain.setTypeface(Typeface.createFromAsset(context.getAssets(),"font/bb.ttf"));

    }

    public int screenWidth() {
        return windowSize.x;
    }
    public int screenHeight() {
        return windowSize.y;
    }

    public int getBallX(){
        return  ballX;
    }

    public int getBallY(){
        return  ballY;
    }

    public int getPaddleX(){
        return  paddleX;
    }

    public int getPaddleY(){
        return  paddleY;
    }

    public void registerColoredObject(ChangeableColor c){
        colorChanger.registerColoredObject(c);
    }

    public void registerObjectToDraw(Drawable d){
        objectsToDraw.add(d);
    }

    public void changeColors(){ colorChanger.changeColors();; }

    public void drawObjects(Canvas canvas){
        for(Drawable d : objectsToDraw){
            d.draw(canvas);
        }
    }

    public void showScore(int computerScore, int playerScore, Canvas canvas) {
        canvas.drawText(Integer.toString(computerScore), screenWidth()/2, screenHeight()/2 - screenWidth()/4, scoreTextPain);
        canvas.drawText(":", screenWidth()/2, screenHeight()/2, scoreTextPain);
        canvas.drawText(Integer.toString(playerScore), screenWidth()/2, screenHeight()/2 + screenWidth()/4, scoreTextPain);
    }
    public void showCalibration(Canvas canvas){
        canvas.drawText("cablirating", screenWidth()/2, screenHeight()/2 - 2*textSize/3, calibrationTextPain);
        canvas.drawText("don't cover", screenWidth()/2, screenHeight()/2, calibrationTextPain);
        canvas.drawText("light sensor", screenWidth()/2, screenHeight()/2 + 2*textSize/3, calibrationTextPain);
    }
}
