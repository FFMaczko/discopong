package com.filipmaczko.discopong;

public class MyColor {
    private int red, green, blue;

    public MyColor(){
        red = 255;
        green = 0;
        blue = 0;
    }

    public MyColor(int r, int g, int b){
        red = r;
        green = g;
        blue = b;
    }

    public int getRed(){
        return red;
    }

    public int getGreen(){
        return green;
    }

    public int getBlue(){
        return blue;
    }

    public void setRed(int r){
        red = r;
        if(red>255){
          red=255;
        }
        if(red<0){
            red=0;
        }
    }

    public void setGreen(int g){
        green = g;
        if(green>255){
            green=255;
        }
        if(green<0){
            green=0;
        }
    }

    public void setBlue(int b){
        blue = b;
        if(blue>255){
            blue=255;
        }
        if(blue<0){
            blue=0;
        }
    }

    public int getColorCode(){
        return 0XFF000000 + red * 65536 + green *256 + blue;
    }
}
