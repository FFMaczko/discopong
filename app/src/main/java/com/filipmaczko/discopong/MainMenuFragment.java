package com.filipmaczko.discopong;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainMenuFragment extends Fragment {

    private TextView graj, sterowanie,autorzy;
    MainMenu parent;

    public void setParent(MainMenu parent)
    {
     this.parent=parent;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragemnt_main_menu, parent, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Display display = MainMenu.activity.getWindowManager().getDefaultDisplay();
        Point windowSize = new Point();
        display.getSize(windowSize);

        graj = getView().findViewById(R.id.graj);
        final Intent it= new Intent(MainMenu.activity, Game.class);
        graj.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(it);
            }
        });
        if(windowSize.x<1080)
        graj.setTextSize(70.0f * (float)(windowSize.x)/1080.0f);
        sterowanie = getView().findViewById(R.id.sterowanie);
        autorzy = getView().findViewById(R.id.a1);
        sterowanie.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                parent.loadedFragment=2;
                parent.selectFragment(2);
            }
        });
        autorzy.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                parent.loadedFragment = 0;
                parent.selectFragment(0);
            }
        });
        if(windowSize.x<1080)   sterowanie.setTextSize(30.0f * (float)(windowSize.x)/1080.0f);
        if(windowSize.x<1080)   autorzy.setTextSize(30.0f * (float)(windowSize.x)/1080.0f);
        MainMenu.registerText(sterowanie);
        MainMenu.registerText(graj);
        MainMenu.registerText(autorzy);
    }

}
