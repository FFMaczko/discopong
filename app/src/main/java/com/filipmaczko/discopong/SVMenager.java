package com.filipmaczko.discopong;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Vibrator;

public class SVMenager {
    private Activity gameActivity;
    //sounds
    private final MediaPlayer bounceSound;
    private final MediaPlayer pointSound;
    //vibrations
    private Vibrator vibrator;
    private final int VIBRATION_SHORT = 50;
    private final int VIBRATION_LONG = 250;

    public SVMenager(Activity context){
        gameActivity = context;
        vibrator = (Vibrator) gameActivity.getSystemService(gameActivity.VIBRATOR_SERVICE);
        bounceSound = MediaPlayer.create(gameActivity, R.raw.bounce);
        pointSound = MediaPlayer.create(gameActivity, R.raw.point);
    }

    public void point(){
        pointSound.start();
        vibrator.vibrate(VIBRATION_LONG);
    }

    public void bounce(){
        bounceSound.start();
        vibrator.vibrate(VIBRATION_SHORT);
    }


}
