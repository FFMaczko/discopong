package com.filipmaczko.discopong;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    private MainThread gameThread;
    private GameManager gm;
    public GameView (Activity context){
        super(context);
        gm=new GameManager(context);
        getHolder().addCallback(this);
        setFocusable(true);
    }

    public void pause(){
        gm.pauseGame();
    }

    public void unpause(){
        gm.unpauseGame();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if(gameThread==null) {
            gameThread = new MainThread(getHolder(), this);
            gameThread.setRunning(true);
            gameThread.start();
        }
        gm.turnOnSensoes();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while (retry) {
            try {
                gameThread.setRunning(false);
                gameThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            retry = false;
        }
        gameThread=null;
        gm.turnOffSensors();
    }

    public void update() {
        gm.update();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(Color.BLACK);
            gm.drawObjects(canvas);
        }
    }
}
