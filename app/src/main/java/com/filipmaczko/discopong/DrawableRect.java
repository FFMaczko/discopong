package com.filipmaczko.discopong;

import android.graphics.Canvas;
import android.graphics.Paint;

public class DrawableRect implements  ChangeableColor, Drawable {
    private Paint myPaint;
    private int x,y, sizeX, sizeY;

    public DrawableRect(int x, int y, int sizeX, int sizeY) {
        this.x = x;
        this.y = y;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        myPaint = new Paint();
        myPaint.setColor(0xFFFF0000);
    }

    public void setPaintColor(int c){
        myPaint.setColor(c);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setMyPaint(Paint newPaint){
        myPaint = newPaint;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawRect(x, y, x + sizeX, y + sizeY, myPaint);
    }

    @Override
    public void changeColor(int colorCode) {
        setPaintColor(colorCode);
    }
}
