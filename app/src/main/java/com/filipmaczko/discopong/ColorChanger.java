package com.filipmaczko.discopong;

import java.util.LinkedList;

public class ColorChanger {

    private LinkedList<ChangeableColor> coloredObjects;
    private MyColor color = new MyColor(255,0,0);
    private  int colorState=0, changeSpeed=5;

    public ColorChanger(){
        coloredObjects = new LinkedList<ChangeableColor>();
    }

    public ColorChanger(int changeSpeed){
        this();
        this.changeSpeed = changeSpeed;
    }

    public void registerColoredObject(ChangeableColor co){
        coloredObjects.add(co);
    }

    public boolean disregisterColoredObject(ChangeableColor co){
        return coloredObjects.remove(co);
    }

    public void changeColors(){
        if(colorState==0) {
            color.setGreen(color.getGreen() + changeSpeed);
            if(color.getGreen()==255){
                colorState+=1;
            }
        }
        if(colorState==1) {
            color.setRed(color.getRed() - changeSpeed);
            if(color.getRed()==0){
                colorState+=1;
            }
        }
        if(colorState==2) {
            color.setBlue(color.getBlue() + changeSpeed);
            if(color.getBlue()==255){
                colorState+=1;
            }
        }
        if(colorState==3) {
            color.setGreen(color.getGreen() - changeSpeed);
            if(color.getGreen()==0){
                colorState+=1;
            }
        }
        if(colorState==4) {
            color.setRed(color.getRed() + changeSpeed);
            if(color.getRed()==255){
                colorState+=1;
            }
        }
        if(colorState==5) {
            color.setBlue(color.getBlue() - changeSpeed);
            if(color.getBlue()==0){
                colorState=0;
            }
        }
        for(ChangeableColor co : coloredObjects){
            co.changeColor(color.getColorCode());
        }
    }

}
